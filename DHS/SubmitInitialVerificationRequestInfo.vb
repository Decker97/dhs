﻿Public Class SubmitInitialVerificationRequestInfo
    Private _clientSoftwareVersion As Decimal

    Private _clientCompanyId As System.Nullable(Of Integer)

    Private _lastName As String

    Private _firstName As String

    Private _middleInitial As String

    Private _otherNamesUsed As String

    Private _ssn As String

    Private _emailAddress As String

    Private _birthDate As Date

    Private _hireDate As Date

    Private _lateVerificationReasonCode As String

    Private _lateVerificationOtherReasonText As String

    Private _citizenshipStatusCode As CitizenshipStatusCode

    Private _alienNumber As String

    Private _i94Number As String

    Private _cardNumber As String

    Private _passportNumber As String

    Private _noForeignPassportIndicator As System.Nullable(Of Boolean)

    Private _countryOfIssuanceCode As String

    Private _visaNumber As String

    Private _documentTypeId As Integer

    Private _listBDocumentTypeId As System.Nullable(Of Integer)

    Private _listCDocumentTypeId As System.Nullable(Of Integer)

    Private _supportingDocumentTypeId As System.Nullable(Of Integer)

    Private _issuingAuthorityCode As String

    Private _listBCDocumentNumber As String

    Private _noDocumentExpirationDateIndicator As System.Nullable(Of Boolean)

    Private _documentExpirationDate As Date

    Private _employerCaseId As String

    Private _submittingOfficialName As String

    Private _submittingOfficialPhoneNumber As String

    Private _submittingOfficialPhoneExtension As String

    Public Property ClientSoftwareVersion As Decimal
        Get
            Return _clientSoftwareVersion
        End Get
        Set(value As Decimal)
            _clientSoftwareVersion = value
        End Set
    End Property

    Public Property ClientCompanyId As Integer?
        Get
            Return _clientCompanyId
        End Get
        Set(value As Integer?)
            _clientCompanyId = value
        End Set
    End Property

    Public Property LastName As String
        Get
            Return _lastName
        End Get
        Set(value As String)
            _lastName = value
        End Set
    End Property

    Public Property FirstName As String
        Get
            Return _firstName
        End Get
        Set(value As String)
            _firstName = value
        End Set
    End Property

    Public Property MiddleInitial As String
        Get
            Return _middleInitial
        End Get
        Set(value As String)
            _middleInitial = value
        End Set
    End Property

    Public Property OtherNamesUsed As String
        Get
            Return _otherNamesUsed
        End Get
        Set(value As String)
            _otherNamesUsed = value
        End Set
    End Property

    Public Property Ssn As String
        Get
            Return _ssn
        End Get
        Set(value As String)
            _ssn = value
        End Set
    End Property

    Public Property EmailAddress As String
        Get
            Return _emailAddress
        End Get
        Set(value As String)
            _emailAddress = value
        End Set
    End Property

    Public Property BirthDate As Date
        Get
            Return _birthDate
        End Get
        Set(value As Date)
            _birthDate = value
        End Set
    End Property

    Public Property HireDate As Date
        Get
            Return _hireDate
        End Get
        Set(value As Date)
            _hireDate = value
        End Set
    End Property

    Public Property LateVerificationReasonCode As String
        Get
            Return _lateVerificationReasonCode
        End Get
        Set(value As String)
            _lateVerificationReasonCode = value
        End Set
    End Property

    Public Property LateVerificationOtherReasonText As String
        Get
            Return _lateVerificationOtherReasonText
        End Get
        Set(value As String)
            _lateVerificationOtherReasonText = value
        End Set
    End Property

    Public Property CitizenshipStatusCode As CitizenshipStatusCode
        Get
            Return _citizenshipStatusCode
        End Get
        Set(value As CitizenshipStatusCode)
            _citizenshipStatusCode = value
        End Set
    End Property

    Public Property AlienNumber As String
        Get
            Return _alienNumber
        End Get
        Set(value As String)
            _alienNumber = value
        End Set
    End Property

    Public Property I94Number As String
        Get
            Return _i94Number
        End Get
        Set(value As String)
            _i94Number = value
        End Set
    End Property

    Public Property CardNumber As String
        Get
            Return _cardNumber
        End Get
        Set(value As String)
            _cardNumber = value
        End Set
    End Property

    Public Property PassportNumber As String
        Get
            Return _passportNumber
        End Get
        Set(value As String)
            _passportNumber = value
        End Set
    End Property

    Public Property NoForeignPassportIndicator As Boolean?
        Get
            Return _noForeignPassportIndicator
        End Get
        Set(value As Boolean?)
            _noForeignPassportIndicator = value
        End Set
    End Property

    Public Property CountryOfIssuanceCode As String
        Get
            Return _countryOfIssuanceCode
        End Get
        Set(value As String)
            _countryOfIssuanceCode = value
        End Set
    End Property

    Public Property VisaNumber As String
        Get
            Return _visaNumber
        End Get
        Set(value As String)
            _visaNumber = value
        End Set
    End Property

    Public Property DocumentTypeId As Integer
        Get
            Return _documentTypeId
        End Get
        Set(value As Integer)
            _documentTypeId = value
        End Set
    End Property

    Public Property ListBDocumentTypeId As Integer?
        Get
            Return _listBDocumentTypeId
        End Get
        Set(value As Integer?)
            _listBDocumentTypeId = value
        End Set
    End Property

    Public Property ListCDocumentTypeId As Integer?
        Get
            Return _listCDocumentTypeId
        End Get
        Set(value As Integer?)
            _listCDocumentTypeId = value
        End Set
    End Property

    Public Property SupportingDocumentTypeId As Integer?
        Get
            Return _supportingDocumentTypeId
        End Get
        Set(value As Integer?)
            _supportingDocumentTypeId = value
        End Set
    End Property

    Public Property IssuingAuthorityCode As String
        Get
            Return _issuingAuthorityCode
        End Get
        Set(value As String)
            _issuingAuthorityCode = value
        End Set
    End Property

    Public Property ListBCDocumentNumber As String
        Get
            Return _listBCDocumentNumber
        End Get
        Set(value As String)
            _listBCDocumentNumber = value
        End Set
    End Property

    Public Property NoDocumentExpirationDateIndicator As Boolean?
        Get
            Return _noDocumentExpirationDateIndicator
        End Get
        Set(value As Boolean?)
            _noDocumentExpirationDateIndicator = value
        End Set
    End Property

    Public Property DocumentExpirationDate As Date
        Get
            Return _documentExpirationDate
        End Get
        Set(value As Date)
            _documentExpirationDate = value
        End Set
    End Property

    Public Property EmployerCaseId As String
        Get
            Return _employerCaseId
        End Get
        Set(value As String)
            _employerCaseId = value
        End Set
    End Property

    Public Property SubmittingOfficialName As String
        Get
            Return _submittingOfficialName
        End Get
        Set(value As String)
            _submittingOfficialName = value
        End Set
    End Property

    Public Property SubmittingOfficialPhoneNumber As String
        Get
            Return _submittingOfficialPhoneNumber
        End Get
        Set(value As String)
            _submittingOfficialPhoneNumber = value
        End Set
    End Property

    Public Property SubmittingOfficialPhoneExtension As String
        Get
            Return _submittingOfficialPhoneExtension
        End Get
        Set(value As String)
            _submittingOfficialPhoneExtension = value
        End Set
    End Property
End Class
