﻿

Imports System.Reflection
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Description
Imports System.ServiceModel.Dispatcher
Imports DHS.EmployerWebServiceV29
Imports DHS.Administration


Public Class DHSServiceAdapter
    Private PASSWORD_MIN_DAYS As Integer = 5

    Private _client As DHS.EmployerWebServiceV29.EmployerWebServiceV29Client
    Private Admin As DHS.Administration.AdministrationClient

    Private _initialized As Boolean
    Private _disposed As Boolean

    Public Sub Initialize()
        Me._client = New EmployerWebServiceV29Client("CustomBinding_IEmployerWebServiceV29")
        Dim creds As PasswordModel = CredentialManager.GetCredentials()
        Me._client.ClientCredentials.UserName.UserName = creds.UserName
        Me._client.ClientCredentials.UserName.Password = creds.Password
        'Me._client.Endpoint.EndpointBehaviors.Add(New BehaviorExtension())

        Me.Admin = New AdministrationClient("WSHttpBinding_IAdministration")
        Me.Admin.ClientCredentials.UserName.UserName = creds.UserName
        Me.Admin.ClientCredentials.UserName.Password = creds.Password
        'Me.Admin.Endpoint.EndpointBehaviors.Add(New BehaviorExtension())

        Me._initialized = True
    End Sub

    Public Function GetDocumentTypes(ByVal info As GetAvailableDocumentTypesRequest) As GetAvailableDocumentTypesResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetAvailableDocumentTypes(info)
    End Function

    Public Function ResubmitSsa(ByVal info As SubmitSsaResubmittalRequest) As SubmitSsaResubmittalResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        'FixContract(info)
        Return Me._client.SubmitSsaResubmittal(info)
    End Function

    Public Function RequestNameReview(ByVal info As SubmitAdditionalVerificationRequest) As SubmitAdditionalVerificationResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.SubmitAdditionalVerification(info)
    End Function

    Public Function GetListBCDocumentTypes(ByVal info As GetListBCDocumentTypesRequest) As GetListBCDocumentTypesResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetListBCDocumentTypes(info)
    End Function

    Public Sub VerifyConnection()
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Me._client.VerifyConnection()
    End Sub

    Public Function GetEmploymentVerificationFields(ByVal info As GetEmploymentVerificationFieldsRequest) As GetEmploymentVerificationFieldsResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetEmploymentVerificationFields(info)
    End Function

    Public Sub ChangePassword(ByVal NewPassword As String)
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        'The mininum use of a password is 5 days, so if they changed it within this time period then thow exception:
        Dim creds As PasswordModel = CredentialManager.GetCredentials()
        If creds.SetOnEverfiy.HasValue Then
            If creds.SetOnEverfiy.Value.AddDays(PASSWORD_MIN_DAYS) > DateTime.Now Then
                Throw New Exception("ERROR - Password has been reset within the last " + PASSWORD_MIN_DAYS.ToString() + " days. You must wait to reset it again.")
            End If
        End If
        Dim req As New DHS.Administration.SetEmployerUserPasswordRequest()
        req.NewUserPassword = NewPassword
        'FixContract(req)
        Admin.SetEmployerUserPassword(req)
    End Sub


    Public Function InitialVerification(info As SubmitInitialVerificationRequest) As SubmitInitialVerificationResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        'Me.FixContract(info)
        Return Me._client.SubmitInitialVerification(info)
    End Function

    Public Function ReVerifyDhs(info As SubmitDhsReverifyRequest) As SubmitDhsReverifyResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        'FixContract(info)
        Return Me._client.SubmitDhsReverify(info)
    End Function
    Public Function ReVerifySsa(info As SubmitSsaReverifyRequest) As SubmitSsaReverifyResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        'FixContract(info)
        Return Me._client.SubmitSsaReverify(info)
    End Function

    Public Function GetResolvedCases(ByVal info As GetResolvedCasesRequest) As GetResolvedCasesResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetResolvedCases(info)
    End Function

    Public Function CloseCase(info As CloseCaseRequest) As CloseCaseResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.CloseCase(info)
    End Function

    Public Function CaseDetails(info As GetCaseDetailsRequest) As GetCaseDetailsResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetCaseDetails(info)
    End Function

    Public Function GetCaseClosureReasons(ByVal info As GetCaseClosureReasonsRequest) As GetCaseClosureReasonsResult
        If (Not Me._initialized) Then
            Throw New InvalidOperationException("Not initialized")
        End If
        Return Me._client.GetCaseClosureReasons(info)
    End Function

    Public Function RetrieveDocumentPhoto(caseNumber As String) As RetrieveDocumentPhotoResult
        Dim request = New RetrieveDocumentPhotoRequest() With {.CaseNumber = caseNumber, .ClientSoftwareVersion = TheSettings.Version}
        '        Me.FixContract(request)
        Return Me._client.RetrieveDocumentPhoto(request)
    End Function

    Public Function ConfirmDocumentPhoto(caseNumber As String, Optional ByVal Confirm As Boolean = True) As ConfirmDocumentPhotoResult
        Return Me._client.ConfirmDocumentPhoto(New ConfirmDocumentPhotoRequest() With {.CaseNumber = caseNumber, .PhotoConfirmedIndicator = Confirm, .ClientSoftwareVersion = TheSettings.Version})
    End Function

    Public Function ContinueDuplicateCaseRequest(caseNumber As String, Optional reason As String = "INCORRECT_DATA") As ContinueDuplicateCaseResult
        Dim request = New ContinueDuplicateCaseRequest() With {.CaseNumber = caseNumber, .ClientSoftwareVersion = TheSettings.Version, .ReasonCode = reason}
        'Me.FixContract(request)
        Return Me._client.ContinueDuplicateCase(request)
    End Function

    Public Function GetLetter(ByVal req As RetrieveLetterRequest) As RetrieveLetterResult
        'Me.FixContract(req)
        Return Me._client.RetrieveLetter(req)
    End Function

    Public Sub ReportTNCNotifiedByDhs(ByVal req As SetDhsTncNotificationRequest)
        'Me.FixContract(req)
        Me._client.SetDhsTncNotification(req)
    End Sub
    Public Function SubmitDhsReferral(ByVal req As SubmitDhsReferralRequest) As SubmitDhsReferralResult
        'Me.FixContract(req)
        Return Me._client.SubmitDhsReferral(req)
    End Function
    Public Sub SubmitSsaReferral(ByVal req As SubmitSsaReferralRequest)
        'Me.FixContract(req)
        Me._client.SubmitSsaReferral(req)
    End Sub
    Public Sub ReportTNCNotifiedBySsa(ByVal req As SetSsaTncNotificationRequest)
        'Me.FixContract(req)
        Me._client.SetSsaTncNotification(req)
    End Sub

    Public Function CloseCase(caseNumber As String, reasonCode As String, currentlyEmployed As Boolean) As CloseCaseResult
        Dim request As New CloseCaseRequest() With {.CaseNumber = caseNumber, .ClosureReasonCode = reasonCode, .ClientSoftwareVersion = My.Application.Info.Version.ToString(), .CurrentlyEmployed = currentlyEmployed}
        'Me.FixContract(request)
        Return Me._client.CloseCase(request)
    End Function


    Public ReadOnly Property Client As EmployerWebServiceV29Client
        Get
            Return Me._client
        End Get
    End Property



    Public ReadOnly Property Initialized As Boolean
        Get
            Return Me._initialized
        End Get
    End Property




End Class
