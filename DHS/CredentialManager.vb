﻿Imports System.IO
Imports Newtonsoft.Json


Public Class CredentialManager

    Private Shared CredentialsStore As String = "EVerifyPassword.Creds.bk"
    Private Shared rand As Random = New Random
    Private Shared lowerCase As String = "abcdefghijklmnopqrstuvwxyz"
    Private Shared upperCase As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    Private Shared nums As String = "1234567890"
    Private Shared specialChars As String = "!@$%*()<>?:;{}+-~"

    Public Shared Sub DeleteCredentials()
        Dim fileName = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore)
        If File.Exists(fileName) Then
            File.Delete(fileName)
        End If

    End Sub

    Public Shared Function GetCredentials() As PasswordModel
        Dim Creds As New PasswordModel
        Dim fileName As String = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore)
        If File.Exists(fileName) Then
            Dim json As String = File.ReadAllText(fileName)
            Creds = JsonConvert.DeserializeObject(Of PasswordModel)(json)
        End If
        Return Creds
    End Function

    Public Shared Sub SetCredentials(ByVal m As PasswordModel)
        Dim fileName As String = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore)
        If File.Exists(fileName) Then
            File.Delete(fileName)
        End If
        Dim json As String = JsonConvert.SerializeObject(m, Formatting.Indented)
        File.WriteAllText(fileName, json)
    End Sub

    Public Shared Function GeneratePassword() As String
        If (TheSettings.Environment = "Production") Then
            Return CredentialManager.GenerateEVerifyPassword
        Else
            Return CredentialManager.GenerateTestPassword
        End If

    End Function

    Public Shared Function GenerateTestPassword() As String
        Dim password As String = String.Empty
        Dim firstChar As String = CredentialManager.GetRandomChar(lowerCase)
        Dim lastChar As String = CredentialManager.GetRandomChar(upperCase)
        Dim numChar As String = CredentialManager.GetRandomChar(nums)
        Dim specialChar As String = CredentialManager.GetRandomChar(specialChars)
        Dim middleRandoms As String = "SELF29"
        password = (firstChar _
                    + (middleRandoms _
                    + (numChar _
                    + (specialChar + lastChar))))
        Return password
    End Function

    Public Shared Function GenerateEVerifyPassword() As String
        Dim password As String = String.Empty
        Dim firstChar As String = CredentialManager.GetRandomChar(lowerCase)
        Dim lastChar As String = CredentialManager.GetRandomChar(upperCase)
        Dim numChar As String = CredentialManager.GetRandomChar(nums)
        Dim specialChar As String = CredentialManager.GetRandomChar(specialChars)
        Dim middleRandoms As String = String.Empty
        Dim middleLength As Integer = rand.Next(4, 11)
        Dim i As Integer = 0
        Do While (i < middleLength)
            middleRandoms = (middleRandoms + CredentialManager.GetRandomChar((lowerCase _
                            + (upperCase _
                            + (nums + specialChars)))))
            i = (i + 1)
        Loop

        password = (firstChar _
                    + (middleRandoms _
                    + (numChar _
                    + (specialChar + lastChar))))
        Return password
    End Function

    Public Shared Function GetRandomChar(ByVal Possible As String) As String
        Return Possible.Substring(rand.Next(0, Possible.Length), 1)
    End Function
End Class