﻿Public Class PasswordModel

    Private m_UserName As String
    Private m_Password As String
    Private m_Created As DateTime
    Private m_SetOnEVerify As DateTime?


    Public Property UserName As String
        Get
            Return m_UserName
        End Get
        Set
            m_UserName = Value
        End Set
    End Property

    Public Property Password As String
        Get
            Return m_Password
        End Get
        Set
            m_Password = Value
        End Set
    End Property

    Public Property Created As DateTime
        Get
            Return m_Created
        End Get
        Set
            m_Created = Value
        End Set
    End Property

    Public Property SetOnEverfiy As DateTime?
        Get
            Return m_SetOnEVerify
        End Get
        Set
            m_SetOnEVerify = Value
        End Set
    End Property
End Class