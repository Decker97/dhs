﻿Public Module ClosureReasonCodes
    ''' <summary>
    ''' The employee continues To work For the employer after receiving an Employment Authorized
    ''' </summary>
    Public Const EELIG = "EELIG"

    ''' <summary>
    '''  The employee continues To work For the employer after receiving a Final Nonconfirmation result.
    ''' </summary>
    Public Const EFNC = "EFNC"

    ''' <summary>
    ''' The employee continues To work For the employer after receiving a No Show result. Employer retains employee
    ''' </summary>
    Public Const ENOACT = "ENOACT"

    ''' <summary>
    ''' The employee continues To work For the employer after choosing Not To contest a Tentative Nonconfirmation.
    ''' </summary>
    Public Const EUNCNT = "EUNCNT"


    ''' <summary>
    ''' The employee was terminated by the employer For receiving a Final Nonconfirmation result The employee no longer works For the employer
    ''' </summary>
    Public Const TRMFNC = "TRMFNC"

    ''' <summary>
    ''' The employee voluntarily quit working For the employer.
    ''' </summary>
    Public Const EQUIT = "EQUIT"

    ''' <summary>
    ''' The employee was terminated by the employer For reasons other than E-Verify.
    ''' </summary>
    Public Const TERM = "TERM"

    ''' <summary>
    '''  The employee was terminated by the employer For receiving a No Show result.
    ''' </summary>
    Public Const NOACT = "NOACT"


    ''' <summary>
    ''' The employee was terminated by the employer For choosing Not To contest a Tentative Nonconfirmation.
    ''' </summary>
    Public Const UNCNT = "UNCNT"

    ''' <summary>
    ''' The Case Is invalid because another Case With the same data already exists.
    ''' </summary>
    Public Const DUP = "DUP"


    ''' <summary>
    ''' The Case Is invalid because the data entered Is incorrect. Other
    ''' </summary>
    Public Const INCDAT = "INCDAT"

    ''' <summary>
    ''' The Case Is being closed because Of technical issues With E-Verify
    ''' </summary>
    Public Const TECISS = "TECISS"

    ''' <summary>
    ''' The case is a duplicate because the employer created a case with the same data within the past 30 days.
    ''' </summary>
    Public Const ISDP = "ISDP"

End Module
