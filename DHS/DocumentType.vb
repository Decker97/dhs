﻿Public Enum DocumentType
    ArrivalDepartureRecord = 11
    PermanentResidentCard = 13
    AlientRegistrationCard = 13
    EmploymentAuthorizationDocument = 17
    ForeignPassport = 24
    ForeignPassportWithTemporaryStamp = 25
    ListBDocument = 28
    ListCDocument = 28
    USPassport = 29
    USPassportCard = 29
End Enum
