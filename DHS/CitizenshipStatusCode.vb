﻿Public Enum CitizenshipStatusCode
    Citizen = 4
    National = 5
    Resident = 6
    AlienAuthorized = 7
End Enum
