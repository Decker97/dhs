﻿Imports System.Configuration

Public Class TheSettings

    Public Shared ReadOnly Property Environment As String
        Get
            Dim env As String = ConfigurationManager.AppSettings("Environment")
            If String.IsNullOrEmpty(env) Then
                Return "Production"
            Else
                Return env
            End If

        End Get
    End Property

    Public Shared ReadOnly Property CredentialsLocation As String
        Get
            Return ConfigurationManager.AppSettings("CredentialsLocation")
        End Get
    End Property

    Public Shared ReadOnly Property Version As String
        Get
            Return "1.0.0.1"
        End Get
    End Property
End Class
