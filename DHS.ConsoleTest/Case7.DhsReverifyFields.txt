{
  "DhsReverifyFieldList": [
    {
      "Code": "ALIEN_NBR",
      "DisplayLabel": "Alien Number",
      "RequiredIndicator": true
    },
    {
      "Code": "CARD_NBR",
      "DisplayLabel": "Document Number",
      "RequiredIndicator": true
    }
  ]
}