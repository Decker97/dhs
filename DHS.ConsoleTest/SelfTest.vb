﻿Imports System.IO
Imports Newtonsoft.Json
Imports DHS.EmployerWebServiceV29



Module SelfTest
    Public Const DATE_FORMAT As String = "yyyy-MM-dd"

    Private Sub ShowMainMenu()
        ' Begin app message
        WriteOutput("Welcome to E-Verify Self-Test Console App")

        WriteOutput("________________________________________________________________________________________________________________________", ConsoleColor.Cyan)
        WriteOutput("<><> SELECT TEST SCENARIO <><> (set console width to 120)", ConsoleColor.Cyan)
        WriteOutput("________________________________________________________________________________________________________________________", ConsoleColor.Cyan)
        WriteOutput("(1) - Case 1: The No Show Case                                            (b) - Verify Connection", ConsoleColor.Cyan)
        WriteOutput("(c) - Case Status                                                         (d) - (open)", ConsoleColor.Cyan)
        WriteOutput("(e) - (open)                                                              (f) - Get Resolved Cases", ConsoleColor.Cyan)
        WriteOutput("(2) - Case 2: The Queuing Case                                            (g) - Close Resolved Cases", ConsoleColor.Cyan)
        WriteOutput("(h) - Close Case 4                                                        (i) - Close Case 2", ConsoleColor.Cyan)
        WriteOutput("(3) - Case 3: The Passport Case                                           (j) - Get Fields for Initial Verification", ConsoleColor.Cyan)
        WriteOutput("(l) - Case 3 Part E                                                       (k) - Close Case 3", ConsoleColor.Cyan)
        WriteOutput("(4) - Case 4: The Naturalized Citizen Case                                (m) - Case 4 D-1", ConsoleColor.Cyan)
        WriteOutput("(n) - (open)                                                              (o) - Get Document Types", ConsoleColor.Cyan)
        WriteOutput("(5) - Case 5: The ReSubmit Case                                           (p) - Get B C Document Types", ConsoleColor.Cyan)
        WriteOutput("(6) - Case 6: The Second Step Case                                        (q) - Case 5 Special", ConsoleColor.Cyan)
        WriteOutput("(7) - Case 7: The Two ReVerify Case                                       (r) - Close Case 5", ConsoleColor.Cyan)
        WriteOutput("(8) - Case 8: The COI Case                                                (s) - Case 6, Part D1", ConsoleColor.Cyan)
        WriteOutput("(9) - Case 9: The Reprocessing Queue Case                                 (t) - Case 6, Part D2", ConsoleColor.Cyan)
        WriteOutput("(a) - Show Username & Password                                            (0) - Change Password", ConsoleColor.Cyan)
        WriteOutput("(u) - Case 7 Part D1                                                      (v) - Case 8 Part D", ConsoleColor.Cyan)
        WriteOutput("(w) - Close Case 1", ConsoleColor.Cyan)
        WriteOutput("(X) - Quit", ConsoleColor.Cyan)

    End Sub

    Sub Main()

        ShowMainMenu()

        Dim info As ConsoleKeyInfo

        While info.KeyChar <> "X"c
            Dim read As ConsoleKeyInfo = Console.ReadKey()
            info = read
            If info.KeyChar = "1"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 1: The No Show Case <><>", ConsoleColor.Green)
                Case1()
            ElseIf info.KeyChar = "d"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> (open) <><>", ConsoleColor.Green)
            ElseIf info.KeyChar = "2"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 2: The Queuing Case <><>", ConsoleColor.Green)
                Case2()
            ElseIf info.KeyChar = "3"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 3: The Passport Case <><>", ConsoleColor.Green)
                Case3()
            ElseIf info.KeyChar = "4"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 4: The Naturalized Citizen Case <><>", ConsoleColor.Green)
                Case4()
            ElseIf info.KeyChar = "5"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 5: The ReSubmit Case <><>", ConsoleColor.Green)
                Case5a()
            ElseIf info.KeyChar = "6"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 6: The Second Step Case <><>", ConsoleColor.Green)
                Case6()
            ElseIf info.KeyChar = "7"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 7: The Two ReVerify Case <><>", ConsoleColor.Green)
                Case7()
            ElseIf info.KeyChar = "8"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 8: The COI Case <><>", ConsoleColor.Green)
                Case8()
            ElseIf info.KeyChar = "9"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 9: The Reprocessing Queue Case <><>", ConsoleColor.Green)
                Case9()
            ElseIf info.KeyChar = "0"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Change Password <><>", ConsoleColor.Green)
                ChangePassword()
            ElseIf info.KeyChar = "a"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Show Username and Password <><>", ConsoleColor.Green)
                ShowUserNameAndPassword()
            ElseIf info.KeyChar = "b"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Verify Connection <><>", ConsoleColor.Green)
                VerifyConnection()
            ElseIf info.KeyChar = "c"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Enter Case Number (1-9) <><>", ConsoleColor.Green)
                Dim read2 As ConsoleKeyInfo = Console.ReadKey()
                GetCaseStatus(read2.KeyChar)
            ElseIf info.KeyChar = "e"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> (open) <><>", ConsoleColor.Green)
            ElseIf info.KeyChar = "f"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Get Resolved Cases <><>", ConsoleColor.Green)
                GetResolvedCasesList()
            ElseIf info.KeyChar = "g"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Resolved Cases Tech Reason <><>", ConsoleColor.Green)
                CloseResolvedCasesTechResonCode()
            ElseIf info.KeyChar = "h"c Then
                Console.WriteLine(vbCrLf)
                CloseCase4()
            ElseIf info.KeyChar = "i"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Case 2 <><>", ConsoleColor.Green)
                CloseCase2()
            ElseIf info.KeyChar = "j"c Then
                Console.WriteLine(vbCrLf)
                GetInitialVerificationFields()
            ElseIf info.KeyChar = "l"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Case 3 <><>", ConsoleColor.Green)
                Case3E()
            ElseIf info.KeyChar = "k"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Case 3 <><>", ConsoleColor.Green)
                CloseCase3()
            ElseIf info.KeyChar = "m"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 4 D-1 <><>", ConsoleColor.Green)
                Case4D1()
            ElseIf info.KeyChar = "n"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> (open) <><>", ConsoleColor.Green)
            ElseIf info.KeyChar = "o"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Get Document Types <><>", ConsoleColor.Green)
                GetDocumentTypes()
            ElseIf info.KeyChar = "p"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Get Document Types <><>", ConsoleColor.Green)
                GetBCDocumentTypes()
            ElseIf info.KeyChar = "q"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 5 Special <><>", ConsoleColor.Green)
                Case5Special()
            ElseIf info.KeyChar = "r"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Case 5 <><>", ConsoleColor.Green)
                CloseCase5()
            ElseIf info.KeyChar = "s"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case 6 Part D1 <><>", ConsoleColor.Green)
                Case6PartD1()
            ElseIf info.KeyChar = "t"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case6 Part D2 <><>", ConsoleColor.Green)
                Case6PartD2()
            ElseIf info.KeyChar = "u"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case7 Part D1 <><>", ConsoleColor.Green)
                Case7PartD1()
            ElseIf info.KeyChar = "v"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Case8 Part D <><>", ConsoleColor.Green)
                Case8PartD()
            ElseIf info.KeyChar = "w"c Then
                Console.WriteLine(vbCrLf)
                WriteOutput("<><> Close Case 1 <><>", ConsoleColor.Green)
                CloseCase1()
            Else
                Continue While
            End If
            Console.ReadKey()
            ShowMainMenu()
        End While
    End Sub

    Private Sub GetCaseStatus(ByVal KeyChar As Char)
        If KeyChar = "1"c Then
            WriteOutput("<><> Get Case 1 Status <><>", ConsoleColor.Green)
            GetStatus("Case1.InitialVerification.txt")
        ElseIf KeyChar = "2"c Then
            WriteOutput("<><> Get Case 2 Status <><>", ConsoleColor.Green)
            GetStatus("Case2.InitialVerification.txt")
        ElseIf KeyChar = "3"c Then
            WriteOutput("<><> Get Case 3 Status <><>", ConsoleColor.Green)
            GetStatus("Case3.InitialVerification.txt")
        ElseIf KeyChar = "4"c Then
            WriteOutput("<><> Get Case 4 Status <><>", ConsoleColor.Green)
            GetStatus("Case4.InitialVerification.txt")
        ElseIf KeyChar = "5"c Then
            WriteOutput("<><> Get Case 5 Status <><>", ConsoleColor.Green)
            GetStatus("Case5.InitialVerification.txt")
        ElseIf KeyChar = "6"c Then
            WriteOutput("<><> Get Case 6 Status <><>", ConsoleColor.Green)
            GetStatus("Case6.InitialVerification.txt")
        ElseIf KeyChar = "7"c Then
            WriteOutput("<><> Get Case 7 Status <><>", ConsoleColor.Green)
            GetStatus("Case7.InitialVerification.txt")
        ElseIf KeyChar = "8"c Then
            WriteOutput("<><> Get Case 8 Status <><>", ConsoleColor.Green)
            GetStatus("Case8.InitialVerification.txt")
        ElseIf KeyChar = "9"c Then
            WriteOutput("<><> Get Case 9 Status <><>", ConsoleColor.Green)
            GetStatus("Case9.InitialVerification.txt")
        End If
    End Sub

    Public Sub GetInitialVerificationFields()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As GetEmploymentVerificationFieldsRequest = New GetEmploymentVerificationFieldsRequest()
            info.CitizenshipStatusCode = CitizenshipStatusCode.Resident
            info.DocumentTypeId = 13
            Dim result As Object = DHSAdapter.GetEmploymentVerificationFields(info)
            SaveObject("Resident.VerificationFields.txt", result)
            WriteOutput(GetObjectText("Resident.VerificationFields.txt"), ConsoleColor.White)
            Return
        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
    End Sub


    Public Sub GetDocumentTypes()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)

            Dim info As GetAvailableDocumentTypesRequest = New GetAvailableDocumentTypesRequest()
            info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
            Dim result As Object = DHSAdapter.GetDocumentTypes(info)
            SaveObject("Citizen.DocumentTypes.txt", result)
            WriteOutput(GetObjectText("Citizen.DocumentTypes.txt"), ConsoleColor.White)
            Return

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try



    End Sub

    Public Sub GetBCDocumentTypes()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            ' 2017-06-15 - Verify Connection throws error
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)

            Dim info As GetListBCDocumentTypesRequest = New GetListBCDocumentTypesRequest()
            info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
            Dim result As Object = DHSAdapter.GetListBCDocumentTypes(info)
            SaveObject("Citizen.BC.DocumentTypes.txt", result)
            WriteOutput(GetObjectText("Citizen.BC.DocumentTypes.txt"), ConsoleColor.White)
            Return

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
            'System.Threading.Thread.Sleep(1000)
        End Try



    End Sub


#Region "Case 1"
    Public Function InitInfo(ByVal info As SubmitInitialVerificationRequest) As SubmitInitialVerificationRequest
        info.AlienNumber = String.Empty
        info.BirthDate = Nothing
        info.CardNumber = String.Empty
        info.CitizenshipStatusCode = String.Empty
        info.ClientCompanyId = Nothing
        info.ClientSoftwareVersion = TheSettings.Version
        info.CountryOfIssuanceCode = String.Empty
        info.DocumentExpirationDate = Nothing
        info.DocumentTypeId = Nothing
        info.EmailAddress = String.Empty
        info.EmployerCaseId = String.Empty
        info.ExtensionData = Nothing
        info.FirstName = String.Empty
        info.HireDate = Nothing
        info.I94Number = String.Empty
        info.IssuingAuthorityCode = String.Empty
        info.LastName = String.Empty
        info.LateVerificationOtherReasonText = String.Empty
        info.LateVerificationReasonCode = String.Empty
        info.ListBCDocumentNumber = String.Empty
        info.ListBDocumentTypeId = Nothing
        info.ListCDocumentTypeId = Nothing
        info.MiddleInitial = String.Empty
        info.NoDocumentExpirationDateIndicator = Nothing
        info.NoForeignPassportIndicator = Nothing
        info.OtherNamesUsed = String.Empty
        info.PassportNumber = String.Empty
        info.Ssn = String.Empty
        info.SubmittingOfficialName = String.Empty
        info.SubmittingOfficialPhoneNumber = String.Empty
        info.SupportingDocumentTypeId = Nothing
        info.VisaNumber = String.Empty
        Return info
    End Function


    Public Function Case1Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.FirstName = "David"
        info.LastName = "Cook"
        info.BirthDate = New DateTime(1935, 12, 3).ToString(DATE_FORMAT)
        info.DocumentTypeId = 28
        info.ListBDocumentTypeId = 1
        info.ListCDocumentTypeId = 13
        info.ListBCDocumentNumber = 870165815
        info.SupportingDocumentTypeId = 1
        info.IssuingAuthorityCode = "MS"
        info.DocumentExpirationDate = New DateTime(2020, 12, 3).ToString(DATE_FORMAT)  '#12/3/2020#
        info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
        info.Ssn = "000000007"
        info.HireDate = New DateTime(2017, 4, 7).ToString(DATE_FORMAT) '#4/7/2017#
        info.EmailAddress = "li.frederick.c@gmail.com"
        info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        Return info
    End Function

    Public Function Case2Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.FirstName = "Don"
        info.LastName = "Jaque"
        info.BirthDate = New DateTime(1989, 12, 12).ToString(DATE_FORMAT)
        info.DocumentTypeId = 28
        info.ListBDocumentTypeId = 2
        info.ListCDocumentTypeId = 13
        info.Ssn = "000000008"
        info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
        info.HireDate = New DateTime(2017, 7, 10).ToString(DATE_FORMAT)
        info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        info.EmailAddress = "decker97@gmail.com"
        Return info
        'info.ListBCDocumentNumber = "MCS1234567890" 'DHS said this field must NOT be specified for some reason
        'info.DocumentExpirationDate = New DateTime(2020, 12, 12).ToString(DATE_FORMAT) 'DHS said this field must NOT be specified for some reason
    End Function

    Public Function Case3Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.FirstName = "Elaine"
        info.LastName = "Goodell"
        info.BirthDate = New DateTime(1977, 6, 9).ToString(DATE_FORMAT)

        info.DocumentTypeId = 29            'Doc type (a) Passport, USA.
        info.PassportNumber = "441209794"
        'info.CountryOfIssuanceCode = "USA"
        'info.NoForeignPassportIndicator = True
        info.DocumentExpirationDate = New DateTime(2018, 4, 6).ToString(DATE_FORMAT)

        info.Ssn = "000000007"
        'info.ListCDocumentTypeId = 13

        'info.ListBDocumentTypeId = Nothing
        'info.ListBCDocumentNumber = Nothing

        info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
        info.HireDate = New DateTime(2017, 7, 16).ToString(DATE_FORMAT)
        info.EmailAddress = "decker97@gmail.com"
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        Return info
    End Function

    Public Function Case4Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.FirstName = "Yankee"
        info.LastName = "Doodle"
        info.BirthDate = New DateTime(1976, 7, 4).ToString(DATE_FORMAT)
        info.DocumentTypeId = 28
        info.ListBDocumentTypeId = 1
        info.ListCDocumentTypeId = 13
        'info.ListBCDocumentNumber = 870165815
        info.SupportingDocumentTypeId = 1
        info.IssuingAuthorityCode = "NY"
        info.DocumentExpirationDate = New DateTime(2020, 12, 30).ToString(DATE_FORMAT)
        info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
        info.Ssn = "000000006"
        info.HireDate = New DateTime(2017, 7, 16).ToString(DATE_FORMAT)
        info.EmailAddress = "decker97@gmail.com"
        info.ClientSoftwareVersion = TheSettings.Version
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        Return info
    End Function


    Public Function Case5Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.LastName = "Hunter"
        info.FirstName = "Olympia"
        info.BirthDate = New DateTime(1976, 3, 4).ToString(DATE_FORMAT)
        info.Ssn = "123456798"
        info.EmailAddress = "decker97@gmail.com"
        info.AlienNumber = "257312373"
        info.DocumentTypeId = 13
        info.CardNumber = "QLC3856718636"
        info.HireDate = New DateTime(2017, 7, 10).ToString(DATE_FORMAT)
        info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"

        'info.DocumentExpirationDate = New DateTime(2022, 11, 7).ToString(DATE_FORMAT)
        'info.NoForeignPassportIndicator = True
        'info.ListCDocumentTypeId = 13
        'info.ListBCDocumentNumber = "123456798"
        info.ClientSoftwareVersion = TheSettings.Version
        info.CitizenshipStatusCode = CitizenshipStatusCode.Resident
        Return info
    End Function

    Public Function Case6Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.LastName = "Alea"
        info.FirstName = "Whitaker"
        info.BirthDate = New DateTime(1954, 11, 5).ToString(DATE_FORMAT)
        info.Ssn = "000000007"
        info.EmailAddress = "decker97@gmail.com"
        info.AlienNumber = "965788697"
        info.DocumentTypeId = 25
        info.PassportNumber = "WHATEVER"
        info.HireDate = New DateTime(2017, 7, 20).ToString(DATE_FORMAT)
        'info.DocumentExpirationDate = New DateTime(2020, 12, 3).ToString(DATE_FORMAT)
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        info.CitizenshipStatusCode = CitizenshipStatusCode.Resident
        Return info
    End Function

    Public Function Case7Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.LastName = "Tim"
        info.FirstName = "Salek"
        info.BirthDate = New DateTime(1979, 12, 12).ToString(DATE_FORMAT)
        info.Ssn = "123456798"
        info.EmailAddress = "decker97@gmail.com"
        info.AlienNumber = "746546546"
        info.DocumentTypeId = 17
        info.CardNumber = "MSC0902850010"
        info.HireDate = New DateTime(2017, 7, 20).ToString(DATE_FORMAT)
        info.DocumentExpirationDate = New DateTime(2020, 12, 12).ToString(DATE_FORMAT)
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        info.CitizenshipStatusCode = CitizenshipStatusCode.AlienAuthorized
        Return info
    End Function

    Public Function Case8Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.LastName = "Aliana"
        info.FirstName = "SATA"
        info.BirthDate = New DateTime(1970, 2, 12).ToString(DATE_FORMAT)
        info.Ssn = "000000007"
        info.EmailAddress = "decker97@gmail.com"
        info.DocumentTypeId = 24
        info.I94Number = "20120070001"
        info.CountryOfIssuanceCode = "AFG"
        info.PassportNumber = "BEL112233"
        info.HireDate = New DateTime(2017, 7, 20).ToString(DATE_FORMAT)
        'info.DocumentExpirationDate = New DateTime(2030, 1, 12).ToString(DATE_FORMAT)
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        info.CitizenshipStatusCode = CitizenshipStatusCode.AlienAuthorized
        Return info
    End Function

    Public Function Case9Info() As SubmitInitialVerificationRequest
        Dim info As New SubmitInitialVerificationRequest
        info = InitInfo(info)
        info.LastName = "Amelia"
        info.FirstName = "Duff"
        info.BirthDate = New DateTime(1990, 1, 25).ToString(DATE_FORMAT)
        info.Ssn = "000000008"
        info.EmailAddress = "decker97@gmail.com"
        info.ListBDocumentTypeId = 5
        info.ListCDocumentTypeId = 16
        info.DocumentTypeId = 28 'B & C Documents.
        info.HireDate = New DateTime(2017, 7, 20).ToString(DATE_FORMAT)
        'info.LateVerificationReasonCode = "LATE_HIRE_DT_RSN_TECH_PROB"
        info.ClientSoftwareVersion = TheSettings.Version
        info.CitizenshipStatusCode = CitizenshipStatusCode.Citizen
        Return info
    End Function

    Private Function FileExists(ByVal sName As String) As Boolean
        Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, sName)
        Return System.IO.File.Exists(fspec)
    End Function

    Private Sub Case1()        ' Case 1: The No Show Case
        Try
            Dim CaseName As String = "Case1.InitialVerification.txt"
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case1Info()
            If FileExists(CaseName) = False Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject(CaseName, result)
            End If
            WriteOutput(GetObjectText(CaseName), ConsoleColor.White)
            Dim status As GetCaseDetailsResult = GetStatus(CaseName)
            Dim res As Result = GetResult(CaseName)

            If status.MessageCode = "41" Then 'Dupe
                ContinueDuplicateCase(res.CaseNumber)
            End If

            If status.MessageCode = "30" Then 'DMV Number incorrect
                Dim reverifyInfo As New SubmitDhsReverifyRequest()
                reverifyInfo.AlienNumber = info.AlienNumber
                reverifyInfo.BirthDate = info.BirthDate
                reverifyInfo.CardNumber = info.CardNumber
                reverifyInfo.CaseNumber = res.CaseNumber
                reverifyInfo.ClientSoftwareVersion = TheSettings.Version
                reverifyInfo.CountryOfIssuanceCode = info.CountryOfIssuanceCode
                reverifyInfo.ExtensionData = Nothing
                reverifyInfo.I94Number = info.I94Number
                reverifyInfo.ListBCDocumentNumber = info.ListBCDocumentNumber
                reverifyInfo.NoForeignPassportIndicator = info.NoForeignPassportIndicator
                reverifyInfo.PassportNumber = info.PassportNumber
                reverifyInfo.VisaNumber = info.VisaNumber
                Dim r2 As SubmitDhsReverifyResult = DHSAdapter.ReVerifyDhs(reverifyInfo)
                SaveObject("Case1.ReVerify.txt", r2)
                WriteOutput(GetObjectText("Case1.Reverify.txt"), ConsoleColor.White)
            Else
                WriteOutput("Incorrect response from Web service, expected 30 and got " + status.MessageCode, ConsoleColor.Yellow)
            End If

            status = GetStatus(CaseName)

            If (status.MessageCode = "29") Then '29 is the data is still incorrect (DL number).
                GeneralPartC_SSA(res.CaseNumber, "Case1")
                WriteOutput("Wait 2 Weeks, then check the Resolution Code for S, then Close Case 1.", ConsoleColor.White)
            Else
                WriteOutput("Incorrect response from Web service, expected 29 and got " + status.MessageCode, ConsoleColor.Yellow)
            End If

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub ContinueDuplicateCase(ByVal CaseNumber As String)
        Dim DHSAdapter As New DHSServiceAdapter()
        DHSAdapter.Initialize()
        WriteOutput("DHS service adapter created", ConsoleColor.Green)
        DHSAdapter.VerifyConnection()
        WriteOutput("Connection verified", ConsoleColor.Green)
        SaveObject("ContinueDupeCase." + CaseNumber + ".txt", DHSAdapter.ContinueDuplicateCaseRequest(CaseNumber))
        WriteOutput(GetObjectText("ContinueDupeCase." + CaseNumber + ".txt"))
    End Sub


    Private Sub GeneralPartC_SSA(ByVal caseNumber As String, ByVal TestCase As String)
        Dim DHSAdapter As New DHSServiceAdapter()
        DHSAdapter.Initialize()
        WriteOutput("DHS service adapter created", ConsoleColor.Green)
        DHSAdapter.VerifyConnection()
        WriteOutput("Connection verified", ConsoleColor.Green)

        'Retrieve the fan letter:
        Dim req As New RetrieveLetterRequest()
        req.CaseNumber = caseNumber
        req.ClientSoftwareVersion = TheSettings.Version
        req.ExtensionData = Nothing
        req.LetterTypeCode = LetterTypeCodeType.SSA_FAN
        SaveLetter("PartC.FANLetter." + TestCase + ".pdf", DHSAdapter.GetLetter(req))
        WriteOutput("Letter Saved to " + "PartC.FANLetter." + TestCase + ".pdf")

        'Report employee notified about the TNC
        Dim req2 As New SetSsaTncNotificationRequest()
        req2.CaseNumber = caseNumber
        req2.ClientSoftwareVersion = TheSettings.Version
        req2.EmployeeNotifiedIndicator = True
        req2.ExtensionData = Nothing
        DHSAdapter.ReportTNCNotifiedBySsa(req2) 'No return value for this.
        WriteOutput("ReportTNCNotifiedBySsa done")

        'Refer the case 
        Dim req3 As New SubmitSsaReferralRequest()
        req3.CaseNumber = caseNumber
        req3.ClientSoftwareVersion = TheSettings.Version
        req3.ExtensionData = Nothing
        DHSAdapter.SubmitSsaReferral(req3)

        'Get the RDC letter
        req.LetterTypeCode = LetterTypeCodeType.SSA_RDC
        SaveLetter("PartC.SsaReferral." + TestCase + ".txt", DHSAdapter.GetLetter(req))
        WriteOutput("Letter Saved to " + "PartC.SsaReferral." + TestCase + ".txt")

    End Sub
    Private Sub GeneralPartC_DHS(ByVal caseNumber As String, ByVal TestCase As String)
        Dim DHSAdapter As New DHSServiceAdapter()
        DHSAdapter.Initialize()
        WriteOutput("DHS service adapter created", ConsoleColor.Green)
        DHSAdapter.VerifyConnection()
        WriteOutput("Connection verified", ConsoleColor.Green)

        'Retrieve the fan letter:
        Dim req As New RetrieveLetterRequest()
        req.CaseNumber = caseNumber
        req.ClientSoftwareVersion = TheSettings.Version
        req.ExtensionData = Nothing
        req.LetterTypeCode = LetterTypeCodeType.DHS_FAN
        SaveLetter("PartC.FANLetter." + TestCase + ".pdf", DHSAdapter.GetLetter(req))
        WriteOutput("Letter Saved to " + "PartC.FANLetter." + TestCase + ".pdf")

        'Report employee notified about the TNC
        Dim req2 As New SetDhsTncNotificationRequest()
        req2.CaseNumber = caseNumber
        req2.ClientSoftwareVersion = TheSettings.Version
        req2.EmployeeNotifiedIndicator = True
        req2.ExtensionData = Nothing
        DHSAdapter.ReportTNCNotifiedByDhs(req2) 'No return value for this.
        WriteOutput("ReportTNCNotifiedByDhs done")

        'Refer the case 
        Dim req3 As New SubmitDhsReferralRequest()
        req3.CaseNumber = caseNumber
        req3.ClientSoftwareVersion = TheSettings.Version
        req3.ExtensionData = Nothing
        SaveObject("PartC.DhsReferralRequest." + TestCase + ".txt", DHSAdapter.SubmitDhsReferral(req3))
        WriteOutput(GetObjectText("PartC.DhsReferralRequest." + TestCase + ".txt"), ConsoleColor.White)

        'Get the RDC letter
        req.LetterTypeCode = LetterTypeCodeType.DHS_RDC
        SaveLetter("PartC.DhsReferral." + TestCase + ".txt", DHSAdapter.GetLetter(req))
        WriteOutput("Letter Saved to " + "PartC.DhsReferral." + TestCase + ".txt")

    End Sub

    Private Sub CloseCase1()
        Dim status As GetCaseDetailsResult = GetStatus("Case1.InitialVerification.txt")
        If status.ResolutionCode = "S" Then 'No Show
            Dim result As Result = GetResult("Case1.InitialVerification.txt")
            CloseCase(result.CaseNumber, "ENOACT", True) 'No Show result, continues to work.
        Else
            WriteOutput("Unexpected Resolution Code, looking for S got " + status.ResolutionCode, ConsoleColor.Yellow)
        End If
    End Sub

    Private Sub CloseCase2()
        Dim status As GetCaseDetailsResult = GetStatus("Case2.InitialVerification.txt")
        If status.MessageCode = "43" Then '42 -> 43 after 24 hours.
            Dim result As Result = GetResult("Case2.InitialVerification.txt")
            CloseCase(result.CaseNumber, "TECISS", True) 'Tech resons with EVerify.
        Else
            WriteOutput("Unexpected Message Code, looking for 43 got " + status.MessageCode, ConsoleColor.Yellow)
        End If
    End Sub

    Private Sub CloseCase3()
        Dim status As GetCaseDetailsResult = GetStatus("Case3.InitialVerification.txt")
        If status.ResolutionCode = "O" Then 'Authorized
            Dim result As Result = GetResult("Case3.InitialVerification.txt")
            CloseCase(result.CaseNumber, "EQUIT", False) 'Employee Quit
        Else
            WriteOutput("Unexpected Resolution Code, looking for O got " + status.ResolutionCode, ConsoleColor.Yellow)
        End If
        'Dim info As GetCaseClosureReasonsRequest = New GetCaseClosureReasonsRequest()
        'info.CaseNumber = result.CaseNumber
        'info.CurrentlyEmployed = False
        'Dim DHSAdapter As New DHSServiceAdapter()
        'DHSAdapter.Initialize()
        'WriteOutput("DHS service adapter created", ConsoleColor.Green)
        'DHSAdapter.VerifyConnection()
        'WriteOutput("Connection verified", ConsoleColor.Green)
        'Dim r As GetCaseClosureReasonsResult = DHSAdapter.GetCaseClosureReasons(info)
        'SaveObject("Case3.ClosureReasons.txt", r)
        'WriteOutput(GetObjectText("Case3.ClosureReasons.txt"), ConsoleColor.White)
    End Sub


    Private Function GetObjectText(ByVal sFile As String) As String
        Dim filename As String = Path.Combine(TheSettings.CredentialsLocation, sFile)
        If File.Exists(filename) Then
            Return File.ReadAllText(filename)
        End If
        Return ""
    End Function

    Private Sub SaveObject(ByVal Key As String, ByVal Data As Object)
        Dim json As String = JsonConvert.SerializeObject(Data, Formatting.Indented)
        Dim filename As String = Path.Combine(TheSettings.CredentialsLocation, Key)
        If (Not filename.ToLower().EndsWith(".txt")) Then
            filename = filename + ".txt"
        End If
        If File.Exists(filename) Then
            File.Delete(filename)
        End If
        File.WriteAllText(filename, json)
    End Sub

    Private Sub SaveLetter(ByVal Key As String, ByVal Data As Object)
        Dim filename As String = Path.Combine(TheSettings.CredentialsLocation, Key)
        If File.Exists(filename) Then
            File.Delete(filename)
        End If
        Dim res As RetrieveLetterResult = Data
        File.WriteAllBytes(filename, res.Letter)
    End Sub

    Private Function GetResult(ByVal Key As String) As Result
        Dim filename As String = Path.Combine(TheSettings.CredentialsLocation, Key)
        Dim json As String = File.ReadAllText(filename)
        Return JsonConvert.DeserializeObject(Of Result)(json)
    End Function

    Private Function GetStatus(ByVal TestCase As String) As GetCaseDetailsResult
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            ' 2017-06-15 - Verify Connection throws error
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim res As Result = GetResult(TestCase)
            Dim info As New GetCaseDetailsRequest()
            info.CaseNumber = res.CaseNumber
            Dim result As GetCaseDetailsResult = DHSAdapter.CaseDetails(info)
            SaveObject("Status." + TestCase + ".txt", result)
            WriteOutput(GetObjectText("Status." + TestCase + ".txt"), ConsoleColor.White)
            Return result
        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
        Return New GetCaseDetailsResult()
    End Function


#End Region

#Region "Case 2"
    Private Sub Case2()        ' Case 2: The Queuing Case
        Try
            Dim CaseName As String = "Case2.InitialVerification.txt"
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case2Info()
            If (FileExists(CaseName) = False) Then
                Dim res As SubmitInitialVerificationResult = DHSAdapter.InitialVerification(info)
                SaveObject(CaseName, res)
            End If
            WriteOutput(GetObjectText(CaseName), ConsoleColor.White)
            Dim status As GetCaseDetailsResult = GetStatus(CaseName)
            Dim result As Result = GetResult(CaseName)
            If status.MessageCode = "41" Then   'Duplicate case, continue dupe:
                ContinueDuplicateCase(result.CaseNumber)
            End If

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
    End Sub
#End Region

#Region "Case 3"
    Private Sub Case3()
        ' Case 3: The Passport Case
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case3Info()
            Dim result As Object = DHSAdapter.InitialVerification(info)
            SaveObject("Case3.InitialVerification.txt", result)
            WriteOutput(GetObjectText("Case3.InitialVerification.txt"), ConsoleColor.White)

            Dim res As Result = GetResult("Case3.InitialVerification.txt")
            If (res.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            'Reverify:
            Dim reverifyInfo As New SubmitDhsReverifyRequest()
            reverifyInfo.AlienNumber = info.AlienNumber
            reverifyInfo.BirthDate = info.BirthDate
            reverifyInfo.CardNumber = info.CardNumber
            reverifyInfo.CaseNumber = res.CaseNumber
            reverifyInfo.ClientSoftwareVersion = TheSettings.Version
            reverifyInfo.CountryOfIssuanceCode = info.CountryOfIssuanceCode
            reverifyInfo.ExtensionData = Nothing
            reverifyInfo.I94Number = info.I94Number
            reverifyInfo.ListBCDocumentNumber = info.ListBCDocumentNumber
            reverifyInfo.NoForeignPassportIndicator = info.NoForeignPassportIndicator
            reverifyInfo.PassportNumber = info.PassportNumber
            reverifyInfo.VisaNumber = info.VisaNumber

            Dim r2 As SubmitDhsReverifyResult = DHSAdapter.ReVerifyDhs(reverifyInfo)
            SaveObject("Case3.Reverify.txt", r2)
            WriteOutput("Case3.Reverify.txt", ConsoleColor.White)

            If (r2.MessageCode = "29") Then
                GeneralPartC_DHS(result.CaseNumber, "Case3.C1")
            End If

            'In the WS GUI, Select Data Verified, Then Yes in the Verify PP data screen, switch 
            'the last two digits of Passport Number to 49. Check status = " then do Case3 Part E

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case3E()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)

            Dim res As Result = GetResult("Case3.InitialVerification.txt")

            'Get the photo:
            Dim r2 As Object = DHSAdapter.RetrieveDocumentPhoto(res.CaseNumber)
            SaveObject("Case3E.DocumentPhoto.txt", r2)
            WriteOutput(GetObjectText("Case3E.DocumentPhoto.txt"), ConsoleColor.White)

            'Report Photo does not match:
            Dim r3 As Object = DHSAdapter.ConfirmDocumentPhoto(res.CaseNumber, False)
            SaveObject("Case3E.DocumentPhotoConfirmFalse.txt", r3)
            WriteOutput(GetObjectText("Case3E.DocumentPhotoConfirmFalse.txt"), ConsoleColor.White)

            'Get the status:
            Dim status As GetCaseDetailsResult = GetStatus("Case3.InitialVerification.txt")
            If status.MessageCode = "29" Then  'Photo does not match
                GeneralPartC_DHS(res.CaseNumber, "Case3.C2")
            End If

            'In the WS GUI, Select Data Verified
            'Check the status for Code O (Employment Verified)
            'Then close the case.

        Catch ex As Exception
            WriteOutput("Error: " & ex.ToString, ConsoleColor.Yellow)
        End Try
    End Sub



#End Region

#Region "Case 4"
    Private Sub Case4()
        ' Case 4: The Naturalized Citizen Case
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case4Info()
            Dim result As Object = DHSAdapter.InitialVerification(info)
            'SaveObject("Case4.InitialVerification.txt", result)
            'WriteOutput(GetObjectText("Case4.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case4.InitialVerification.txt")
            If (res.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            Dim status As GetCaseDetailsResult = GetStatus("Case4.InitialVerification.txt")
            If status.MessageCode = "27" Then
                'If status.EligibilityStatementDetails.ToUpper() = "UNABLE TO CONFIRM CITIZENSHIP." Then
                '"The citizenship status selected for this employee did not match SSA records."
                GeneralPartC_DHS(res.CaseNumber, "Case4")
            End If
            'End If

            'Follow instructions for the WS GUI.
            'Do Case 4 Part D1 next.
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case4D1()
        ' Case 4: The Naturalized Citizen Case
        ' Part D-1
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim status As GetCaseDetailsResult = GetStatus("Case4.InitialVerification.txt")
            Dim res As Result = GetResult("Case4.InitialVerification.txt")
            If status.MessageCode = "27" Then
                For Each rc As ResolvedCase In GetResolvedCasesList()
                    If rc.CaseNumber = res.CaseNumber Then
                        'I can't find Referral Reason anywhere in the docs.
                        'Can't check it. TODO:
                        'if rc.ReferralReason = "Employeee Referred to SSA"
                    End If
                Next
            End If

            'No action to take, referred to SSA.
            'Follow instructions for Part D-2.
            'Do Case 4 Part D2 next.
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub CloseCase4()
        Dim status As GetCaseDetailsResult = GetStatus("Case4.InitialVerification.txt")
        If status.MessageCode = "28" Then 'Final Non-Confirmation
            Dim result As Result = GetResult("Case4.InitialVerification.txt")
            CloseCase(result.CaseNumber, "TRMFNC", False) 'Employee Termination after FNC.
        Else
            WriteOutput("Incorrect Message Code, expected 28, got " + status.MessageCode, ConsoleColor.Yellow)
        End If
    End Sub

#End Region

#Region "Case 5"
    Private Sub Case5a()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case5Info()
            'Dim result As Object = DHSAdapter.InitialVerification(info)
            'SaveObject("Case5.InitialVerification.txt", result)
            WriteOutput(GetObjectText("Case5.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case5.InitialVerification.txt")
            If (res.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            Dim status As GetCaseDetailsResult = GetStatus("Case5.InitialVerification.txt")
            'If status.MessageCode = "36" Then   'SSN does not match
            '    'Re-Verify without correcting SSN
            '    Dim reverifyInfo As New SubmitSsaReverifyRequest()
            '    reverifyInfo.BirthDate = info.BirthDate
            '    reverifyInfo.CaseNumber = res.CaseNumber
            '    reverifyInfo.ClientSoftwareVersion = TheSettings.Version
            '    reverifyInfo.ExtensionData = Nothing
            '    reverifyInfo.Ssn = info.Ssn
            '    reverifyInfo.LastName = info.LastName
            '    reverifyInfo.FirstName = info.FirstName
            '    Dim r2 As EmployerWebServiceReference.SubmitSsaReverifyResult = DHSAdapter.ReVerifySsa(reverifyInfo)
            '    SaveObject("Case5a.Reverify.txt", r2)
            '    WriteOutput(GetObjectText("Case5a.Reverify.txt"), ConsoleColor.White)
            '    If (r2.MessageCode = "27") Then 'SSN Info still does not match
            GeneralPartC_SSA(res.CaseNumber, "Case5a")
            ' End If
            'End If

            'In the GUI, choose Advance Case button and select Review and Resubmit (Review and Update Employee Data).
            'Do case 5, part Special

        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case5Special()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim i As SubmitInitialVerificationRequest = Case5Info()
            Dim res As Result = GetResult("Case5.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case5.InitialVerification.txt")
            If (status.MessageCode = "35") Then
                Dim info As New SubmitSsaResubmittalRequest()
                info.BirthDate = i.BirthDate
                info.CaseNumber = res.CaseNumber
                info.ClientSoftwareVersion = TheSettings.Version
                info.ExtensionData = Nothing
                info.FirstName = i.FirstName
                info.LastName = i.LastName
                info.Ssn = "000000007"
                Dim r As Object = DHSAdapter.ResubmitSsa(info)
                SaveObject("Case5Special.ResubmitSsa", r)
                WriteOutput(GetObjectText("Case5Special.ResubmitSsa"), ConsoleColor.White)
            End If

            'Get status, should now be 30 (Card Number incorrect)
            status = GetStatus("Case5.InitialVerification.txt")
            If status.MessageCode = "30" Then
                'Reverify with this doc number:
                Dim info2 As New SubmitDhsReverifyRequest()
                'info2.BirthDate = i.BirthDate
                info2.CaseNumber = res.CaseNumber
                info2.ClientSoftwareVersion = TheSettings.Version
                info2.ExtensionData = Nothing
                info2.AlienNumber = i.AlienNumber
                info2.CardNumber = "QLC3856718663"
                Dim r2 As Object = DHSAdapter.ReVerifyDhs(info2)
                SaveObject("Case5Special.ReverifyDhs", r2)
                WriteOutput(GetObjectText("Case5Special.ReverifyDhs"), ConsoleColor.White)
            End If

            status = GetStatus("Case5.InitialVerification.txt")
            If status.MessageCode = "31" Then 'Photo Matching required.
                'Document Photo must first be retrieved.
                Dim r4 As Object = DHSAdapter.RetrieveDocumentPhoto(res.CaseNumber)
                SaveObject("Case5Special.DocumentPhoto", r4)
                Dim r3 As Object = DHSAdapter.ConfirmDocumentPhoto(res.CaseNumber, False)
                SaveObject("Case5Special.ConfirmPhotoFalse", r3)
                WriteOutput(GetObjectText("Case5Special.ConfirmPhotoFalse"), ConsoleColor.White)
            End If

            status = GetStatus("Case5.InitialVerification.txt")
            If status.MessageCode = "29" Then 'Photo does not match
                GeneralPartC_DHS(res.CaseNumber, "Case5.C2")
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try

    End Sub

    Private Sub CloseCase5()
        Dim status As GetCaseDetailsResult = GetStatus("Case5.InitialVerification.txt")
        Dim res As Result = GetResult("Case5.InitialVerification.txt")
        If status.ResolutionCode = "O" Then        'Authorized to work, after going though WS GUI.
            CloseCase(res.CaseNumber, "TERM", False)
        End If
    End Sub
#End Region

#Region "Case 6"
    Private Sub Case6() 'The Second Step Case
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case6Info()
            Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, "Case6.InitialVerification.txt")
            If (System.IO.File.Exists(fspec) = False) Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject("Case6.InitialVerification.txt", result)
            End If
            WriteOutput(GetObjectText("Case6.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case6.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case6.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case6.InitialVerification.txt")
            If status.MessageCode = "30" Then   'Alien Number incorrect
                'Re-Verify without correcting Alient Number
                Dim reverifyInfo As New SubmitDhsReverifyRequest()
                reverifyInfo.AlienNumber = info.AlienNumber
                'reverifyInfo.BirthDate = info.BirthDate
                reverifyInfo.CaseNumber = res.CaseNumber
                reverifyInfo.ClientSoftwareVersion = TheSettings.Version
                reverifyInfo.ExtensionData = Nothing
                reverifyInfo.CardNumber = info.CardNumber
                reverifyInfo.PassportNumber = info.PassportNumber
                Dim r2 As SubmitDhsReverifyResult = DHSAdapter.ReVerifyDhs(reverifyInfo)
                SaveObject("Case6a.Reverify.txt", r2)
                WriteOutput(GetObjectText("Case6a.Reverify.txt"), ConsoleColor.White)

            End If
            status = GetStatus("Case6.InitialVerification.txt")
            If status.MessageCode = "05" Then   'DHS Review in process
                'In the GUI, choose Case in Continuance
                'Do case 6, pard D1
                WriteOutput("Status code is correct, in the WS GUI choose Case in Continuance. Then do Case 6, part D1.", ConsoleColor.White)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case6PartD1()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim i As SubmitInitialVerificationRequest = Case6Info()
            Dim res As Result = GetResult("Case6.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case6.InitialVerification.txt")
            If (status.ResolutionCode = "C") Then 'Case in Continuance
                WriteOutput("Correct Resolution code, in the WS GUI select Data Not Verfified then do Case 6 Part D2.", ConsoleColor.White)
                Return
            Else
                WriteOutput("Incorrect Resolution code (" + status.ResolutionCode + ")", ConsoleColor.Yellow)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case6PartD2()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim i As SubmitInitialVerificationRequest = Case6Info()
            Dim res As Result = GetResult("Case6.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case6.InitialVerification.txt")
            If (status.ResolutionCode = "U") Then 'Case in Continuance
                CloseCase(res.CaseNumber, "UNCNT", False) 'Close Case, choosing not to contest.
                Return
            Else
                WriteOutput("Incorrect Resolution code (" + status.ResolutionCode + ")", ConsoleColor.Yellow)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub



#End Region

#Region "Case 7"
    Private Sub Case7()        ' Case 7: The Two ReVerify Case
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case7Info()
            Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, "Case7.InitialVerification.txt")
            If (System.IO.File.Exists(fspec) = False) Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject("Case7.InitialVerification.txt", result)
            End If
            WriteOutput(GetObjectText("Case7.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case7.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case7.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case7.InitialVerification.txt")
            If status.MessageCode = "36" Then   'SSA does not match
                'Re-Verify without correcting Alient Number
                Dim reverifyInfo As New SubmitSsaReverifyRequest()
                reverifyInfo.Ssn = info.Ssn
                reverifyInfo.BirthDate = info.BirthDate
                reverifyInfo.CaseNumber = res.CaseNumber
                reverifyInfo.ClientSoftwareVersion = TheSettings.Version
                reverifyInfo.ExtensionData = Nothing
                reverifyInfo.FirstName = info.FirstName
                reverifyInfo.LastName = info.LastName
                Dim r2 As SubmitSsaReverifyResult = DHSAdapter.ReVerifySsa(reverifyInfo)
                SaveObject("Case7a.Reverify.txt", r2)
                WriteOutput(GetObjectText("Case7a.Reverify.txt"), ConsoleColor.White)
            End If

            status = GetStatus("Case7.InitialVerification.txt")
            If status.MessageCode = "27" Then   'SSA still no match.
                GeneralPartC_SSA(res.CaseNumber, "Case7")
                WriteOutput("In the WS GUI, select Data Verified. Then do Case 7 Part D1.", ConsoleColor.White)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case7PartD1()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case7Info()
            Dim res As Result = GetResult("Case7.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case7.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case7.InitialVerification.txt")
            If status.MessageCode = "30" Then   'Card Number does not match
                'Re-Verify without correcting Alient Number
                Dim reverifyInfo As New SubmitDhsReverifyRequest()
                reverifyInfo.CardNumber = "MSC0902850001"
                'reverifyInfo.DocumentExpirationDate = info.DocumentExpirationDate
                'reverifyInfo.BirthDate = info.BirthDate
                reverifyInfo.CaseNumber = res.CaseNumber
                reverifyInfo.ClientSoftwareVersion = TheSettings.Version
                reverifyInfo.AlienNumber = info.AlienNumber
                Dim r2 As SubmitDhsReverifyResult = DHSAdapter.ReVerifyDhs(reverifyInfo)
                SaveObject("Case7a.Reverify2.txt", r2)
                WriteOutput(GetObjectText("Case7a.Reverify2.txt"), ConsoleColor.White)
            End If

            status = GetStatus("Case7.InitialVerification.txt")
            If status.MessageCode = "31" Then   'Photo Match required.
                'Get the photo:
                Dim r2 As Object = DHSAdapter.RetrieveDocumentPhoto(res.CaseNumber)
                SaveObject("Case7.DocumentPhoto.txt", r2)
                WriteOutput(GetObjectText("Case7.DocumentPhoto.txt"), ConsoleColor.White)

                'Report Photo matches:
                Dim r3 As Object = DHSAdapter.ConfirmDocumentPhoto(res.CaseNumber, True)
                SaveObject("Case7.DocumentPhotoConfirmTrue.txt", r3)
                WriteOutput(GetObjectText("Case7.DocumentPhotoConfirmTrue.txt"), ConsoleColor.White)
            End If

            status = GetStatus("Case7.InitialVerification.txt")
            If status.MessageCode = "8" Then 'Authorized.
                CloseCase(res.CaseNumber, "INCDAT", False)
            End If

        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub
#End Region

#Region "Case 8"
    Private Sub Case8()        ' Case 8: The COI Case
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case8Info()
            Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, "Case8.InitialVerification.txt")
            If (System.IO.File.Exists(fspec) = False) Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject("Case8.InitialVerification.txt", result)
            End If
            WriteOutput(GetObjectText("Case8.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case8.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case8.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case8.InitialVerification.txt")
            If status.MessageCode = "30" Then   'Country Of Issuance (COI) is incorrect.
                Dim reverifyInfo As New SubmitDhsReverifyRequest()
                reverifyInfo.CountryOfIssuanceCode = "BEL"
                reverifyInfo.I94Number = info.I94Number
                reverifyInfo.PassportNumber = info.PassportNumber
                reverifyInfo.CaseNumber = res.CaseNumber
                'reverifyInfo.BirthDate = info.BirthDate
                reverifyInfo.DocumentExpirationDate = info.DocumentExpirationDate
                reverifyInfo.ListBCDocumentNumber = info.ListBCDocumentNumber
                reverifyInfo.NoForeignPassportIndicator = info.NoForeignPassportIndicator
                reverifyInfo.ClientSoftwareVersion = TheSettings.Version
                Dim r2 As SubmitDhsReverifyResult = DHSAdapter.ReVerifyDhs(reverifyInfo)
                SaveObject("Case8a.Reverify.txt", r2)
                WriteOutput(GetObjectText("Case8a.Reverify.txt"), ConsoleColor.White)
            End If

            status = GetStatus("Case8.InitialVerification.txt")
            If status.MessageCode = "8" Then   'Authorized
                'Request a name review
                Dim info2 As New SubmitAdditionalVerificationRequest()
                info2.CaseNumber = res.CaseNumber
                info2.ClientSoftwareVersion = TheSettings.Version
                Dim r3 As SubmitAdditionalVerificationResult = DHSAdapter.RequestNameReview(info2)
                SaveObject("Case8.NameReview.txt", r3)
                WriteOutput(GetObjectText("Case8.NameReview.txt"), ConsoleColor.White)
                Console.WriteLine()
                WriteOutput("In the WSM GUI, select Data Not Verified for this case, then do Case 8 Part D.", ConsoleColor.White)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub

    Private Sub Case8PartD()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case8Info()
            Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, "Case8.InitialVerification.txt")
            If (System.IO.File.Exists(fspec) = False) Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject("Case8.InitialVerification.txt", result)
            End If
            WriteOutput(GetObjectText("Case8.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case8.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case8.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case8.InitialVerification.txt")
            If status.ResolutionCode = "U" Then   'Name Review resulted in tnc
                'Close Case as invalid because another case with same data exists.
                CloseCase(res.CaseNumber, "DUP", False)
            End If

        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try

    End Sub
#End Region

#Region "Case 9"
    Private Sub Case9()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim info As SubmitInitialVerificationRequest = Case9Info()
            Dim fspec As String = Path.Combine(TheSettings.CredentialsLocation, "Case9.InitialVerification.txt")
            If (System.IO.File.Exists(fspec) = False) Then
                Dim result As Object = DHSAdapter.InitialVerification(info)
                SaveObject("Case9.InitialVerification.txt", result)
            End If
            WriteOutput(GetObjectText("Case9.InitialVerification.txt"), ConsoleColor.White)
            Dim res As Result = GetResult("Case9.InitialVerification.txt")
            Dim status As GetCaseDetailsResult = GetStatus("Case9.InitialVerification.txt")
            If (status.MessageCode = "41") Then
                ContinueDuplicateCase(res.CaseNumber)
            End If

            status = GetStatus("Case9.InitialVerification.txt")
            If status.MessageCode = "42" Then   'Put in Reprocessing que.
                CloseCase(res.CaseNumber, "DUP", False)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub
#End Region

#Region "Change Password"
    Private Sub ChangePassword()
        ' ChangePassword
        Try
            Dim m As New PasswordModel()
            'ManageCredentials.DeleteCredentials()
            m = CredentialManager.GetCredentials()
            If (String.IsNullOrEmpty(m.UserName)) Then
                m.UserName = "ABOY4080"
                m.Password = "FlashX30!a"
                m.Created = DateTime.Now
                CredentialManager.SetCredentials(m)
                Return
            End If
            m.Password = CredentialManager.GeneratePassword()
            m.Created = DateTime.Now
            Dim adapter As New DHSServiceAdapter()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            adapter.Initialize()
            WriteOutput("DHS service adapter initialized", ConsoleColor.Green)
            adapter.VerifyConnection()
            WriteOutput("DHS service adapter connection verified", ConsoleColor.Green)
            adapter.ChangePassword(m.Password)
            WriteOutput("Password changed successfully", ConsoleColor.Green)
            m.SetOnEverfiy = DateTime.Now
            CredentialManager.SetCredentials(m)
            WriteOutput("Password saved to credentials file", ConsoleColor.Green)
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
    End Sub
#End Region

#Region "Show Username & Password"
    Private Sub ShowUserNameAndPassword()
        Try
            Dim m As PasswordModel = CredentialManager.GetCredentials()
            WriteOutput("UserName : " + m.UserName, ConsoleColor.White)
            WriteOutput("Password : " + m.Password, ConsoleColor.White)
            WriteOutput("Created  : " + m.Created.ToString("MM/dd/yyyy hh:mm:ss tt"), ConsoleColor.White)
            If (m.SetOnEverfiy.HasValue) Then
                WriteOutput("Set on EV: " + m.SetOnEverfiy.Value.ToString("MM/dd/yyyy hh:mm:ss tt"), ConsoleColor.White)
            End If
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
            System.Threading.Thread.Sleep(5000)
        End Try
    End Sub
#End Region

#Region "Verify Connection"
    Private Sub VerifyConnection()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            ' 2017-06-15 - Verify Connection throws error
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
            System.Threading.Thread.Sleep(5000)
        End Try
    End Sub
#End Region

    Public Sub CloseResolvedCasesTechResonCode()
        CloseResolvedCases("TECISS", True)
    End Sub

    Public Sub CloseCase(ByVal CaseNumber As String, ByVal Reason As String, ByVal Employed As Boolean)
        Dim DHSAdapter As New DHSServiceAdapter()
        DHSAdapter.Initialize()
        WriteOutput("DHS service adapter created", ConsoleColor.Green)
        ' 2017-06-15 - Verify Connection throws error
        DHSAdapter.VerifyConnection()
        WriteOutput("Connection verified", ConsoleColor.Green)
        Dim req As New CloseCaseRequest()
        req.ExtensionData = Nothing
        req.CaseNumber = CaseNumber
        req.ClientSoftwareVersion = TheSettings.Version
        req.ClosureReasonCode = Reason
        req.CurrentlyEmployed = Employed
        SaveObject("CloseCase - " + CaseNumber + ".txt", DHSAdapter.CloseCase(req))
        WriteOutput(GetObjectText("CloseCase - " + CaseNumber + ".txt"), ConsoleColor.White)
    End Sub


    Public Sub CloseResolvedCases(ByVal ReasonCode As String, ByVal Employed As Boolean)
        Try
            Dim list As List(Of ResolvedCase) = GetResolvedCasesList()
            Dim item As New ResolvedCase
            For Each item In list
                CloseCase(item.CaseNumber, ReasonCode, Employed)
            Next
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
            System.Threading.Thread.Sleep(5000)
        End Try
    End Sub


    Public Sub GetResolvedCases()
        Try
            Dim DHSAdapter As New DHSServiceAdapter()
            DHSAdapter.Initialize()
            WriteOutput("DHS service adapter created", ConsoleColor.Green)
            ' 2017-06-15 - Verify Connection throws error
            DHSAdapter.VerifyConnection()
            WriteOutput("Connection verified", ConsoleColor.Green)
            Dim req As New GetResolvedCasesRequest()
            req.ExtensionData = Nothing
            req.NumberOfRecords = 100
            SaveObject("ResolvedCases.txt", DHSAdapter.GetResolvedCases(req))
            WriteOutput(GetObjectText("ResolvedCases.txt"))
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
            System.Threading.Thread.Sleep(5000)
        End Try
    End Sub


    Public Function GetResolvedCasesList() As List(Of ResolvedCase)
        Try
            GetResolvedCases()
            Dim json As String = GetObjectText("ResolvedCases.txt")
            Dim ret As ListOfResovledCases = JsonConvert.DeserializeObject(Of ListOfResovledCases)(json)
            Return ret.CaseList
        Catch ex As Exception
            WriteOutput("Error: " & ex.Message.ToString, ConsoleColor.Yellow)
        End Try
        Return New List(Of ResolvedCase)
    End Function

    Public Class ListOfResovledCases
        Public CaseList As List(Of ResolvedCase)
    End Class

    Public Class ResolvedCase
        Public CaseNumber As String
        Public VerificationStep As Integer
        Public EmployerCaseId As String
        Public CurrentStateCode As String
        Public MessageCode As String
        Public ResolutionCode As String
        Public EligibilityStatement As String
        Public ResolvedDate As String
        Public LetterTypeCodeList As Object
    End Class


    Public Class Letter
        Public Letter As Byte()
    End Class


    Public Class Result
        Public CaseNumber As String
        Public CurrentStateCode As String
        Public MessageCode As Integer
        Public EligibilityStatement As String
        Public EligibilityStatementDetails As String
        Public SystemLastName As String
        Public SystemFirstName As String
        Public LetterTypeCodeList As Object
    End Class


#Region "Helper Object"
    Sub WriteOutput(ByVal message As String, Optional ByVal fontColor As ConsoleColor = ConsoleColor.White)
        If ConfigurationManager.GetSection("appSettings")("displayOutput") = True Then
            Console.ForegroundColor = fontColor
            Console.WriteLine(message)
        End If
    End Sub
#End Region
End Module
