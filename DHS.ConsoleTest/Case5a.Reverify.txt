{
  "CurrentStateCode": "PRC2",
  "MessageCode": 27,
  "EligibilityStatement": "SSA Tentative Nonconfirmation (TNC)",
  "EligibilityStatementDetails": "The Social Security number entered in E-Verify was not valid according to SSA records.",
  "SystemLastName": "",
  "SystemFirstName": "",
  "LetterTypeCodeList": [
    0
  ]
}