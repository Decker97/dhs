﻿using System.Configuration;
namespace eVerifyPasswordGenerator
{
    public class TheSettings
    {
        public static string Environment
        {
            get
            {
                string env = ConfigurationManager.AppSettings["Environment"];
                if (string.IsNullOrEmpty(env))
                    return "Production";
                else
                    return env;
            }
        }

        public static string CredentialsLocation
        {
            get
            {
                return ConfigurationManager.AppSettings["CredentialsLocation"];
            }
        }

        public static string Version
        {
            get
            {
                return "1.0.0.1";
            }
        }

    }
}
