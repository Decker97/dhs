﻿using System;

namespace eVerifyPasswordGenerator
{
    public class PasswordModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime Created { get; set; }
        public DateTime? SetOnEverfiy { get; set; }
    }
}
