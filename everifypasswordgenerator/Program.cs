﻿using System;

namespace eVerifyPasswordGenerator
{
    class Program
    {

        static void Main(string[] args)
        {
            int MaxLength = 0;
            int MinLength = 20;
            for (int i = 0; i < 10000; i++)
            {
                string password = CredentialManager.GenerateEVerifyPassword();
                Console.WriteLine(password.Length.ToString("00") + "    " + password);
                if (password.Length > MaxLength)
                    MaxLength = password.Length;
                if (password.Length < MinLength)
                    MinLength = password.Length;
            }
            Console.WriteLine("Max Length = " + MaxLength.ToString("00"));
            Console.WriteLine("Min Length = " + MinLength.ToString("00"));
            Console.ReadKey();
            PasswordModel m = new PasswordModel();
            m.Password = CredentialManager.GenerateEVerifyPassword();
            Console.WriteLine(m.Password);
            m.Created = DateTime.Now;
            m.UserName = "mbravo";
            ManageCredentials.SaveEverifyCredentialsToFile(m);
            m = ManageCredentials.GetCredentialsFromFile();
            Console.WriteLine(m.Password);
            Console.ReadKey();
        }

    }
}

