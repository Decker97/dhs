﻿using System;

namespace eVerifyPasswordGenerator
{
  public class CredentialManager
    {
        private static Random rand = new Random();
        private static string lowerCase = "abcdefghijklmnopqrstuvwxyz";
        private static string upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static string nums = "1234567890";
        private static string specialChars = "!@$%*()<>?:;{}+-~";

        public static PasswordModel GetCredentials()
        {
            return ManageCredentials.GetCredentialsFromFile();
        }

        public static void SetCredentials(PasswordModel m)
        {
            ManageCredentials.SaveEverifyCredentialsToFile(m);
        }

        public static string GeneratePassword()
        {
            if (TheSettings.Environment == "Production")
                return GenerateEVerifyPassword();
            else
                return GenerateTestPassword();
        }

        public static string GenerateTestPassword()
        {
            return "v29te$tA";
            //string password = string.Empty;
            //string firstChar = GetRandomChar(lowerCase);
            //string lastChar = GetRandomChar(upperCase);
            //string numChar = GetRandomChar(nums);
            //string specialChar = GetRandomChar(specialChars);
            //string middleRandoms = "SELF29";
            //password = firstChar + middleRandoms + numChar + specialChar + lastChar;
            //return password;
        }


        public static string GenerateEVerifyPassword()
        {
            string password = string.Empty;
            string firstChar = GetRandomChar(lowerCase);
            string lastChar = GetRandomChar(upperCase);
            string numChar = GetRandomChar(nums);
            string specialChar = GetRandomChar(specialChars);
            string middleRandoms = string.Empty;

            int middleLength = rand.Next(4, 11);

            for (int i = 0; i < middleLength; i++)
            {
                middleRandoms += GetRandomChar(lowerCase + upperCase + nums + specialChars);
            }
            password = firstChar + middleRandoms + numChar + specialChar + lastChar;
            return password;
        }

        public static string GetRandomChar(string Possible)
        {
            return Possible.Substring(rand.Next(0, Possible.Length), 1);
        }
    }
}
