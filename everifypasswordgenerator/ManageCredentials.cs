﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace eVerifyPasswordGenerator
{
    public class ManageCredentials
    {
        private const string CredentialsStore = "EVerifyPassword.Creds.bk";

        public static void DeleteCredentials()
        {
            var fileName = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore);
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        //public static void CopyCredsToSource()
        //{
        //    string source = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), CredentialsStore);
        //    string dest = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore);
        //    if (File.Exists(source))
        //    {
        //        if (File.Exists(dest))
        //        {
        //            File.Delete(dest);
        //        }
        //        File.Copy(source, dest);
        //    }
        //}

        public static PasswordModel GetCredentialsFromFile()
        {
            var Creds = new PasswordModel();
            var fileName = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore);
            if (File.Exists(fileName))
            {
                Creds = GetCredentialsFromFile(fileName);
            }
            return Creds;
        }

        public static void SaveEverifyCredentialsToFile(PasswordModel c)
        {
            var fileName = Path.Combine(TheSettings.CredentialsLocation, CredentialsStore);
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            var json = JsonConvert.SerializeObject(c, Formatting.Indented);
            File.WriteAllText(fileName, json);
        }


        private static PasswordModel GetCredentialsFromFile(string fileName)
        {
            var json = File.ReadAllText(fileName);
            return JsonConvert.DeserializeObject<PasswordModel>(json);
        }

        private static void SaveCredentialsToFile(string fileName, PasswordModel c)
        {
            var json = JsonConvert.SerializeObject(c, Formatting.Indented);
            //var cypherText = json.ToSecureString().EncryptString();
            File.WriteAllText(fileName, json);
        }
    }
}
